const _setup = require('./modules/_setup');
const _authentication = require('./modules/_authentication');
const _app = require('./modules/_app');

const log = require('./vendor/logger');
const inpt = require('./vendor/inpt')();

inpt.on('clear | c', () => {
    log.clear();
});

inpt.on('exit | e', () => {
    process.exit(0);
});

_setup(_session => {
    console.log();

    _session._connections = {};

    _session._app._http.except('/u/*', response => {
        response.head(302, {
            'Location': `/?o=${response.url.substr(3)}`
        });

        response.emit();
    });

    _session._app.on('connection', s => {
        let cuo;
        let p = false;

        log.info('New connection.');

        s.on('session--end', () => {
            if(p) {
                _session._connections[cuo.set('profile').u] = '';
            }

            s.close();
        });

        _authentication(_session, s, cu => {
            log.success('User logged in.');

            p = true;
            cuo = cu;

            _session._connections[cuo.set('profile').u] = s;

            _app(_session, s, cu);
        });
    });
});