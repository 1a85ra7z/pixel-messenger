const fs = require('fs');
const path = require('path');
const chalk = require('chalk/types');

let hfl = -1;

let read = (f, c) => {
    fs.readFile(path.join(__dirname, `../${f}`), {encoding: 'utf-8'}, (e, d) => {
        if(!e) {
            c(d);
        }
    });
};

let rn = () => {
    let h = {};

    read('data.hook', d => {
        h = JSON.parse(d);

        h.hook.forEach((f, i) => {
            fs.watchFile(`../${f.path}`, () => {
                h.hook[i].version++;

                fs.writeFile('../data.hook', JSON.stringify(h, null, 2), (e) => {
                    if(!e) {
                        console.log(`${chalk.yellow(f.path)} ${chalk.grey('changed')}`);
                    }
                });

                read('version.hook', d => {
                    fs.writeFile('../version.hook',`${`${d.split(':')[0].replace(d.split(':')[0].split('.')[d.split(':')[0].split('.').length - 1], '')}${(parseInt(d.split(':')[0].split('.')[d.split(':')[0].split('.').length - 1]) + 1).toString()}`}:${d.split(':')[1]}`, () => {});
                });
            });
        });

        fs.watchFile(`../js/vendor/hook.js`, () => {
            console.log(chalk.magenta('hook.js changed'));

            read('version.hook', d => {
                fs.writeFile('../version.hook', `${d.split(':')[0]}:${`${d.split(':')[1].replace(d.split(':')[1].split('.')[d.split(':')[1].split('.').length - 1], '')}${(parseInt(d.split(':')[1].split('.')[d.split(':')[1].split('.').length - 1]) + 1).toString()}`}`, () => {});
            });
        });
    });
};

let cch = () => {
    read('data.hook', d => {
        if(hfl !== d.split('\n').length) {
            if(hfl !== -1) {
                console.log(chalk.red('data.hook changed'));
            } else {
                console.log(chalk.blue('Started watcher'));
            }

            rn();
            hfl = d.split('\n').length;
        }
    });
};

fs.watchFile('../data.hook', () => {
    cch();
});

cch();