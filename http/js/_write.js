document.querySelector('.app-chat-write-field--value').setAttribute(
    'style',
    `height:calc(${document.querySelector('.app-chat-write-field--value').scrollHeight}px);`
);

hook.listen(document.querySelector('.app-chat-write-field--value'), 'input', () => {
    document.querySelector('.app-chat-write-field--value').setAttribute(
        'style',
        `height:auto;`
    );

    document.querySelector('.app-chat-write-field--value').setAttribute(
        'style',
        `height:calc(${document.querySelector('.app-chat-write-field--value').scrollHeight}px);`
    );
});

hook.listen(document.querySelector('.app-chat-write--field'), 'click', e => {
    if(e.target !== document.querySelector('.app-chat-write-field--send')) {
        document.querySelector('.app-chat-write-field--value').focus();
    } else {
        done({
            type: 'text',
            data: document.querySelector('.app-chat-write-field--value').value
        });

        document.querySelector('.app-chat-write-field--value').value = '';
    }
});