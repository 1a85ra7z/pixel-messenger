let dpa = false;

hook.run('js:message', () => {}, __parameters);

document.querySelector('.app-sidebar-user-about-name--text').innerHTML = __parameters.name;
document.querySelector('.app-sidebar-user-about-id--text').innerHTML = `${__parameters.id}`;

document.querySelector('.app-sidebar-user--picture').setAttribute('src', __parameters.picture);

hook.listen(
    document.querySelector('.app-sidebar-user-about-id--text'),
    'click',
    () => {
        hook.get('modal:copyId', crrd => {
            hook.run('js:modal', () => {
                document.querySelector('.modal-copyId--input').select();

                hook.listen(
                    document.querySelector('.modal-copyId--copy'),
                    'click', () => {
                        document.querySelector('.modal-copyId--input').select();
                        document.execCommand('copy');

                        render.close('component:modal', () => {
                            hook.run('js:toast', () => {}, {text: 'Copied to clipboard!'});
                        });
                    }
                );
            }, {
                content: crrd,
                variables: {
                    id: `${window.location.href.split('/').slice(0, 3).join('/')}/u/${__parameters.id}`
                }
            });
        });
    },
    ['page:app']
);

hook.listen(
    document.querySelector('.app-sidebar--drag'),
    'mousedown',
    () => {
        dpa = true;
    },
    ['page:app']
);

hook.listen(
    document,
    'mouseup',
    () => {
        dpa = false;
    },
    ['page:app']
);

hook.listen(
    document,
    'mousemove',
    (e) => {
        if(dpa && Math.floor((e.clientX / window.innerWidth) * 100) > 5 && Math.floor((e.clientX / window.innerWidth) * 100) < 95) {
            document.querySelector('.app')
                .setAttribute('style', `grid-template-columns: ${Math.floor((e.clientX / window.innerWidth) * 100)}% auto;`);
        }
    },
    ['page:app']
);

hook.listen(
    document.querySelector('.app-sidebar-user-about-name-holder--icon'),
    'click',
    () => {
        if(
            !document.querySelector('.app-sidebar-user-about-name-holder--tooltip').classList.contains('-state--visible') &&
            !document.querySelector('.app-sidebar-user-about-name-holder--icon').classList.contains('-state--clicked')
        ) {
            document.querySelector('.app-sidebar-user-about-name-holder--tooltip').classList.add('-state--visible');
            document.querySelector('.app-sidebar-user-about-name-holder--icon').classList.add('-state--clicked');
        } else if(
            document.querySelector('.app-sidebar-user-about-name-holder--tooltip').classList.contains('-state--visible') &&
            document.querySelector('.app-sidebar-user-about-name-holder--icon').classList.contains('-state--clicked')
        ) {
            document.querySelector('.app-sidebar-user-about-name-holder--tooltip').classList.remove('-state--visible');
            document.querySelector('.app-sidebar-user-about-name-holder--icon').classList.remove('-state--clicked');
        }
    },

    ['page:app']
);

hook.listen(
    document, 'click',
    e => {
        if(
            document.querySelector('.app-sidebar-user-about-name-holder--tooltip').classList.contains('-state--visible') &&
            document.querySelector('.app-sidebar-user-about-name-holder--icon').classList.contains('-state--clicked') &&
            !document.querySelector('.app-sidebar-user-about-name-holder--tooltip').contains(e.target) &&
            !document.querySelector('.app-sidebar-user-about-name-holder--icon').contains(e.target)
        ) {
            document.querySelector('.app-sidebar-user-about-name-holder--tooltip').classList.remove('-state--visible');
            document.querySelector('.app-sidebar-user-about-name-holder--icon').classList.remove('-state--clicked');
        }
    },

    ['page:app']
);

hook.listen(
    document.querySelector('.app-sidebar-user-about-name-holder-tooltip--logout'),
    'click',
    () => {
        localStorage.setItem('authentication--token', '');
        window.location.reload();
    }
);

hook.listen(
    document.querySelector('.app-sidebar-user-about-name-holder-tooltip--lock'),
    'click',
    () => {
        hook.run('js:lock', () => {});
    }
);

hook.listen(
    document.querySelector('.app-sidebar-user-about-name-holder-tooltip--settings'),
    'click',
    () => {
        render.show('page:settings', () => {
            hook.run('js:settings', () => {
                render.close('page:settings');
            }, __parameters);
        }, {
            layer: true,
            animation: 'fill'
        });
    }
);

setTimeout(() => {
    if(typeof hook.url.parameters.o === 'string') {
        hook.get('modal:newChat', crrd => {
            s.emit('message--getUsername', hook.url.parameters.o);

            s.once(`message-getUsername-response--${hook.url.parameters.o}`, cmd => {
                if(typeof cmd === 'string') {
                    if(!cmd.startsWith('error-message--')) {
                        hook.run('js:modal', () => {
                            hook.listen(document.querySelector('.modal-newChat--start'), 'click', () => {
                                s.emit('message--newChat', hook.url.parameters.o);
                                render.close('component:modal');
                            });

                            hook.listen(document.querySelector('.modal-newChat--abort'), 'click', () => {
                                render.close('component:modal');
                            });
                        }, {
                            content: crrd,
                            variables: {
                                id: hook.url.parameters.o,
                                name: cmd
                            }
                        });
                    } else {
                        let aem = '';

                        switch(cmd.split('error-message--')[1]) {
                            case 'invalidId':
                                aem = 'Invalid ID. Please try again.';
                                break;
                            case 'ownId':
                                aem = `You can't chat with yourself.`;
                                break;
                        }

                        hook.run('js:toast', () => {}, {type: 'error', text: aem})
                    }
                }
            });
        });
    }
}, 10);