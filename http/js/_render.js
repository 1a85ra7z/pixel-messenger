let rl = [];
let rlo = [];
let cc = 0;

let nl = (n) => {
    if (n === undefined) {
        n = cc;
    }

    if (!document.querySelector(`.render--${n}`)) {
        let cr = document.createElement('div');

        cr.classList.add(`render--${n}`);
        cr.classList.add(`render--layer`);

        document.querySelector('.render').appendChild(cr);

        rl.push(
            document.querySelector(`.render--${n}`)
        );

        cc++;
    }

    return rl[n];
};

nl();

let render = {
    show: (f, c, o) => {
        hook.clearIntervals();
        hook.clearTimeouts();

        hook.log.say(`Rendering %c${f}`, {prefix: 'render.js', importance: 6, prefixColor: '#05c46b'}, 'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#05c46b;');

        rlo.push(f);

        let dfo = {
            variables: {},
            clickOutsideElement: '',
            transition: false,
            layer: false,
            animation: '',
            animationCurtain: '',
            fill: true,
            pointer: true,
            curtain: false,
            showtime: -1,
            classes: []
        };

        if (typeof o === 'object') {
            if (typeof o.transition !== 'boolean') {
                o.transition = dfo.transition;
            }

            if (typeof o.layer !== 'boolean') {
                o.layer = dfo.layer;
            }

            if (typeof o.animation !== 'string') {
                o.animation = dfo.animation;
            }

            if (typeof o.animationCurtain !== 'string') {
                o.animationCurtain = dfo.animationCurtain;
            }

            if (typeof o.fill !== 'boolean') {
                o.fill = dfo.fill;
            }

            if (typeof o.curtain !== 'boolean') {
                o.curtain = dfo.curtain;
            }

            if (typeof o.pointer !== 'boolean') {
                o.pointer = dfo.pointer;
            }

            if (typeof o.variables !== 'object') {
                o.variables = dfo.variables;
            }

            if (typeof o.clickOutsideElement !== 'string') {
                o.clickOutsideElement = dfo.clickOutsideElement;
            }

            if (typeof o.showtime !== 'number') {
                o.showtime = dfo.showtime;
            }

            if (typeof o.classes !== 'object') {
                o.classes = dfo.classes;
            }
        } else {
            o = dfo;
        }

        let t = performance.now();

        if (document.querySelector('.curtain').classList.contains('-state--hidden') && o.transition) {
            document.querySelector('.curtain').classList.remove('-state--hidden');
        } else if (!document.querySelector('.curtain').classList.contains('-state--hidden') && !o.transition) {
            document.querySelector('.curtain').classList.add('-state--hidden');
        }

        hook.get(f, d => {
            if(typeof o === 'object' ? typeof o.variables === 'object' : false) {
                if(Object.keys(o.variables).length > 0) {
                    Object.keys(o.variables).forEach(ck => {
                        d = d.replace(`%${ck}`, o.variables[ck]);
                    })
                }
            }

            let ce;

            if (o.layer) {
                ce = nl();
            } else {
                ce = nl(0);
            }

            if(ce !== undefined) {
                ce.innerHTML = d;
                ce.classList.add(`-state--animate${o.animation.substr(0, 1).toUpperCase() + o.animation.substr(1)}`);
                ce.classList.add(`-state--animateCurtain${o.animationCurtain.substr(0, 1).toUpperCase() + o.animationCurtain.substr(1)}`);

                o.classes.forEach(cceil => {
                   ce.classList.add(cceil);
                });

                if(o.fill) {
                    ce.classList.add('-state--fill');
                }

                if(o.curtain) {
                    ce.classList.add('-state--curtain');

                    if(o.animationCurtain.length > 0) {
                        setTimeout(() => {
                            ce.classList.add('-state--curtainVisible');
                        }, 10);
                    } else {
                        ce.classList.add('-state--curtainVisible');
                    }
                }

                if(o.pointer) {
                    ce.classList.add('-state--pointer');
                }

                if(o.clickOutsideElement.length > 0) {
                    if(typeof ce.querySelector(o.clickOutsideElement) !== 'undefined') {
                        hook.listen(
                            ce,
                            'click',
                            (e) => {
                                if(!ce.querySelector(o.clickOutsideElement).contains(e.target)) {
                                    render.close(f);
                                }
                            },
                            [f]
                        )
                    }
                }

                hook.timeout(() => {
                    ce.classList.add('-state--visible');
                }, 10);

                hook.setup();

                if (o.transition) {
                    setTimeout(() => {
                        document.querySelector('.curtain').classList.add('-state--animate');

                        setTimeout(() => {
                            document.querySelector('.curtain').classList.add('-state--hidden');
                            document.querySelector('.curtain').classList.remove('-state--animate');
                        }, 1000);

                        c();

                        if(o.showtime > 0) {
                            setTimeout(() => {
                                render.close(f);
                            }, o.showtime)
                        }
                    }, 3500 - (performance.now() - t))
                } else if (typeof c === 'function') {
                    c();

                    if(o.showtime > 0) {
                        setTimeout(() => {
                            render.close(f);
                        }, o.showtime)
                    }
                }
            }
        });
    },

    close: (component, callback) => {
        if(typeof component === 'string') {
            component = rlo.indexOf(component);
        }

        if(component < rl.length && component > -1) {
            if(component === 0) {
                rl[component].innerHTML = '';
            } else {
                let rfar = () => {
                    rl[component].remove();

                    rl.splice(component, 1);
                    rlo.splice(component, 1);

                    cc--;

                    if(typeof callback === 'function') {
                        callback();
                    }
                };

                if(rl[component].classList.contains('-state--visible') && !rl[component].classList.contains('-state--animate')) {
                    rl[component].classList.add('-state--return');
                    rl[component].classList.remove('-state--visible');

                    hook.timeout(() => {
                        rfar();
                    }, 300);
                } else {
                    rfar();
                }
            }
        }
    },

    component: (n, c, cllb) => {
        hook.get(n, clb => {
            if(typeof c === 'object') {
                if(Object.keys(c).length > 0) {
                    Object.keys(c).forEach(ck => {
                        clb = clb.replace(`%${ck}`, c[ck]);
                    })
                }
            }

            let cc = document.createElement('template');
            cc.innerHTML = clb;

            cllb(cc.content.firstChild);
        });
    }
};