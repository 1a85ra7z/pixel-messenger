let cre;

let fs = (ce, i) => {
    hook.get(`page:settings:${ce}`, (d) => {
        cre = ce;

        document.querySelector('.settings--current').innerHTML = d;
        document.querySelector('.settings-categories--option.-state--active') !== null ? document.querySelector('.settings-categories--option.-state--active').classList.remove('-state--active') : null;

        switch(i) {
            case 0:
                document.querySelector('.settings-current-container-changeImage--image')
                    .setAttribute('src',
                        __parameters.picture
                    );

                document.querySelector('.settings-current-container--username').querySelector('input').setAttribute('value', __parameters.name);
                document.querySelector('.settings-current-container--fullName > input').value = __parameters.fullName;
                document.querySelector('.settings-current-container--email > .component-input--alignCenter > input').value = __parameters.email;

                hook.listen(document.querySelector('.settings-current-container-changeImage--upload'), 'click', () => {
                    document.querySelector('.settings-current-container-changeImage--input').click();

                    document.querySelector('.settings-current-container-changeImage--input').addEventListener('change', (cie) => {
                        cpimage(cie.target.files[0], (cpi) => {
                            s.emit('settings--updateProfilePicture', cpi);
                        });
                    })
                });

                hook.listen(document.querySelector('.settings-current-container-changeImage--reset'), 'click', () => {
                    s.emit('settings--resetProfilePicture');
                });

                hook.listen(document.querySelector('.settings-current-container--fullName > input'), 'input', () => {
                    document.querySelector('.settings-current-container--fullName > input').value =
                        document.querySelector('.settings-current-container--fullName > input').value
                            .replace(/[^a-zA-Z\-\W]/g, '');

                    if(document.querySelector('.settings-current-container--fullName > input').value.length > 0) {
                        s.emit('settings--updateFullName', document.querySelector('.settings-current-container--fullName > input').value);
                    }
                });

                hook.listen(document.querySelector('.settings-current-container--email > .component-input--alignCenter > input'), 'input', () => {
                    if(
                        document.querySelector('.settings-current-container--email > .component-input--alignCenter > input').value.length > 0 &&
                        /^\S+@\S+$/.test(document.querySelector('.settings-current-container--email > .component-input--alignCenter > input').value)
                    ) {
                        s.emit('settings--updateEmail', document.querySelector('.settings-current-container--email > .component-input--alignCenter > input').value);
                    }
                });

                break;
        }
    });
};

document.querySelectorAll('.settings-categories--option').forEach((ce, i) => {
    hook.listen(ce, 'click', () => {
        if(!ce.classList.contains('-state--active')) {
            fs(ce.querySelector('.settings-categories-option-text--name').innerHTML.toLowerCase(), i);
            ce.classList.add('-state--active');
        }
    });

    if(i === 0) {
        fs(ce.querySelector('.settings-categories-option-text--name').innerHTML.toLowerCase(), i);
        ce.classList.add('-state--active');
    }
});

s.on('settings-updateProfilePicture--response', uppd => {
    if(typeof uppd === 'string') {
        if(rlo.includes('page:app')) {
            document.querySelector('.app-sidebar-user--picture').setAttribute('src', uppd);
        }

        if(rlo.includes('page:settings') && cre === 'profile') {
            document.querySelector('.settings-current-container-changeImage--image').setAttribute('src', uppd);
        }
    }
});

hook.listen(document.querySelector('.settings--close'), 'click', () => {
    done();
});

hook.listen(document, 'keydown', e => {
    if(e.which === 27) {
        done();
    }
}, ['page:settings'], true);