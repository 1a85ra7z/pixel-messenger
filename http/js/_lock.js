let sll = () => {
    s.on('authentication--loggedIn', () => {
        localStorage.setItem('lock--token', '');

        s.emit('app--getUserData');

        s.once('app--getUserData', d => {
            render.show('page:app', () => {
                hook.run('js:app', () => {}, d);
            }, false);
        });
    });

    s.once('authentication--token', t => {
        localStorage.setItem('authentication--token', t);
    });


    s.once('connection', () => {
        let e = document.querySelector(`.component--input > input`);
        document.querySelector(`.component--input > input`).focus();

        let sb = () => {
            if(
                (
                    localStorage.getItem('lock--token') !== undefined &&
                    localStorage.getItem('lock--token') !== null
                ) ? localStorage.getItem('lock--token').length > 0 : false
            ) {
                s.emit('authentication', {
                    u: localStorage.getItem('lock--token'),
                    p: e.value,
                    s: (localStorage.getItem('authentication--stayLoggedIn') === 'true')
                });
            } else {
                localStorage.setItem('lock--token', '');
                localStorage.setItem('authentication--token', '');

                window.location.reload();
            }
        };

        document.addEventListener('keypress', ev => {
            if(
                ev.key === 'Enter' &&
                !document.querySelector('.component--button').classList.contains('-state--disabled') &&
                __componentId === 'js:lock' &&
                new RegExp(`[\\w]{6,}`).test(e.value)
            ) {
                sb();
            }
        });

        e.addEventListener('input', () => {
            if(new RegExp(`[\\w]{6,}`).test(e.value)) {
                if(document.querySelectorAll('.component--input')[0].classList.contains('-state--invalid')) {
                    document.querySelectorAll('.component--input')[0].classList.remove('-state--invalid');
                    document.querySelectorAll('.component--input')[0].querySelector('.component-input-header--status').innerHTML = '';
                }

                if(document.querySelector('.component--button').classList.contains('-state--disabled')) {
                    document.querySelector('.component--button').classList.remove('-state--disabled');
                }
            } else if(!s && !document.querySelector('.component--button').classList.contains('-state--disabled')) {
                document.querySelector('.component--button').classList.add('-state--disabled');
            }
        });

        hook.listen(document.querySelector('.authentication-main--unlock'), 'click', () => {
            sb();
        });

        hook.listen(document.querySelector('.authentication-main--logout'), 'click', () => {
            localStorage.setItem('lock--token', '');
            localStorage.setItem('authentication--token', '');
            hook.reload();
            window.location.reload();
        });

        s.on('authentication--wrongPassword', () => {
            if(!document.querySelectorAll('.component--input')[0].classList.contains('-state--invalid')) {
                document.querySelectorAll('.component--input')[0].classList.add('-state--invalid');

                document.querySelectorAll('.component--input')[0].querySelector('.component-input-header--status').innerHTML = ' &mdash; Invalid password';
            }
        });
    });
};

if(
    (
        localStorage.getItem('lock--token') !== undefined &&
        localStorage.getItem('lock--token') !== null
    ) ? localStorage.getItem('lock--token').length > 0 : false
) {
    sll();
} else {
    s.emit('lock--token');

    s.on('lock--token', d => {
        localStorage.setItem('lock--token', d);
        localStorage.setItem('authentication--token', '');
        hook.reload();
    });
}