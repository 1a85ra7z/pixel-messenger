render.show('component:modal', () => {
    done();
}, {
    variables: {
        c: typeof __parameters.content === 'string' ? __parameters.content : '',
        ...__parameters.variables
    },

    layer: true,
    curtain: true,

    animation: 'fill',
    animationCurtain: 'fade',

    clickOutsideElement: '.component-modal--content'
});