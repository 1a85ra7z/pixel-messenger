let s = Socket('wss://localhost:8998');

s.once('connection', () => {
    s.once('authentication', () => {
        hook.run('js:authentication', () => {
            s.emit('app--getUserData');

            s.once('app--getUserData', d => {
                render.show('page:app', () => {
                    hook.run('js:app', () => {}, d);
                }, {transition: !hook.dev});
            });
        });
    });
});

s.on('timeout', () => {
    render.show('page:error:noConnectionToServer')
});