render.show('component:toast', () => {
    if(typeof __parameters.type === 'string') {
        document.querySelector('.component--toast').classList.add(`-state--${__parameters.type}`)
    }

    done();
}, {
    variables: {
        c: typeof __parameters.text === 'string' ? __parameters.text : ''
    },

    layer: true,
    pointer: false,

    animation: 'fade',
    showtime: typeof __parameters.time === 'number' ? (__parameters.time > 0 ? __parameters.time : 2500) : 2500
});