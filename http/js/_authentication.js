let pdl = () => {
    render.show('page:authentication', () => {
        document.querySelector(`.component--input > input`).focus();

        let fcs = false;
        let p = ['', ''];

        let sb = () => {
            localStorage.setItem('authentication--stayLoggedIn', document.querySelector('.component--switch').querySelector('input').checked.toString());

            s.emit('authentication', {
                u: p[0],
                p: p[1],
                s: document.querySelector('.component--switch').querySelector('input').checked
            });
        };

        document.addEventListener('keypress', e => {
            if(
                e.key === 'Enter' &&
                fcs &&
                !document.querySelector('.component--button').classList.contains('-state--disabled') &&
                __componentId === 'js:authentication'
            ) {
                sb();
            }
        });

        [...document.querySelectorAll(`.component--input > input`)].forEach((e, i) => {
            e.addEventListener('focus', () => {
                fcs = true;
            });

            e.addEventListener('focusout', () => {
                fcs = false;
            });

            e.addEventListener('input', () => {
                if(new RegExp(`[\\w]{${(i + 1) * 3},}`).test(e.value)) {
                    p[i] = e.value;
                } else {
                    p[i] = '';
                }

                let s = true;

                p.forEach(ce => {
                    if(!ce) {
                        s = false;
                    }
                });

                if(s) {
                    if(document.querySelectorAll('.component--input')[1].classList.contains('-state--invalid')) {
                        document.querySelectorAll('.component--input')[1].classList.remove('-state--invalid');
                        document.querySelectorAll('.component--input')[1].querySelector('.component-input-header--status').innerHTML = '';
                    }

                    if(document.querySelector('.component--button').classList.contains('-state--disabled')) {
                        document.querySelector('.component--button').classList.remove('-state--disabled');
                    }
                } else if(!s && !document.querySelector('.component--button').classList.contains('-state--disabled')) {
                    document.querySelector('.component--button').classList.add('-state--disabled');
                }
            });
        });

        document.querySelector('.component--button').addEventListener('click', () => {
            sb();
        });

        s.on('authentication--wrongPassword', () => {
            if(!document.querySelectorAll('.component--input')[1].classList.contains('-state--invalid')) {
                document.querySelectorAll('.component--input')[1].classList.add('-state--invalid');

                document.querySelectorAll('.component--input')[1].querySelector('.component-input-header--status').innerHTML = ' &mdash; Invalid password';
            }
        });
    }, false);
};

if((
    localStorage.getItem('authentication--token') !== undefined &&
    localStorage.getItem('authentication--token') !== null
) ? localStorage.getItem('authentication--token').length > 0 : false) {
    s.emit('authentication--autoLogin', localStorage.getItem('authentication--token'));

    s.on('authentication--autoLoginFailed', () => {
        pdl();
    });
} else if((
    localStorage.getItem('lock--token') !== undefined &&
    localStorage.getItem('lock--token') !== null
) ? localStorage.getItem('lock--token').length > 0 : false) {
    render.show('page:lock', () => {
        hook.run('js:lock', () => {});
    });
}  else {
    pdl();
}

s.once('authentication--token', t => {
    localStorage.setItem('authentication--token', t);
});

s.once('authentication--loggedIn', () => {
    log.say('Authenticated', {importance: 4});

    done();
});