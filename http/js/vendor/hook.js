(() => {
    console.clear();
    let lma = [];

    window.log = {
        say: (t, o, c) => {
            let ot = t;

            if(typeof o !== 'object') {
                o = {};
            }

            if(o.importance === undefined) {
                o.importance = 0;
            }

            if(typeof t === 'string') {
                let rs = t.split('%r').length > 1;
                t = t.replace('%r', '%c');

                if(typeof o === 'object' ? (typeof o.showPrefix === 'boolean' ? (o.showPrefix === true) : true) : true) {
                    console[typeof o === 'object' ? (typeof o.type === 'string' ? o.type : 'log') : 'log'](`%c[%c${typeof o === 'object' ? (typeof o.prefix === 'string' ? o.prefix : 'app') : 'app'}%c]: %c${t}`,
                        'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#30303A;',
                        `letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:${typeof o !== 'object' ? '#3C40C6' : (typeof o.prefixColor === 'string' ? o.prefixColor : '#3C40C6')};`,
                        'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#30303A;',
                        `letter-spacing:1.5px;font-family:sans-serif;font-size:1.2em;color:${typeof o !== 'object' ? '#30303A' : (typeof o.color === 'string' ? o.color : '#30303A')};`,
                        c !== undefined ? c : '',
                        rs ? `letter-spacing:1.5px;font-family:sans-serif;font-size:1.2em;color:${typeof o !== 'object' ? '#30303A' : (typeof o.color === 'string' ? o.color : '#30303A')};` : ''
                    );
                } else {
                    console[typeof o === 'object' ? (typeof o.type === 'string' ? o.type : 'log') : 'log'](`%c${t}`,
                        `letter-spacing:1.5px;font-family:sans-serif;font-size:1.2em;color:${typeof o !== 'object' ? '#30303A' : (typeof o.color === 'string' ? o.color : '#30303A')};`,
                        c !== undefined ? c : '',
                        rs ? `letter-spacing:1.5px;font-family:sans-serif;font-size:1.2em;color:${typeof o !== 'object' ? '#30303A' : (typeof o.color === 'string' ? o.color : '#30303A')};` : ''
                    );
                }
            } else {
                if(typeof o === 'object' ? (typeof o.showPrefix === 'boolean' ? (o.showPrefix === true) : true) : true) {
                    console.group(`%c[%c${typeof o === 'object' ? (typeof o.prefix === 'string' ? o.prefix : 'app') : 'app'}%c]:%c OBJECT`,
                        'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#30303A;',
                        `letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:${typeof o !== 'object' ? '#3C40C6' : (typeof o.prefixColor === 'string' ? o.prefixColor : '#3C40C6')};`,
                        'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#30303A;',
                        `letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:${typeof o !== 'object' ? '#3C40C6' : (typeof o.prefixColor === 'string' ? o.prefixColor : '#3C40C6')};`
                    );

                    console.log(t);
                    console.groupEnd();
                }
            }

            if(typeof o !== 'object' ? true : (typeof o.remove !== 'boolean' ? true : !o.remove)) {
                lma.push({
                    t: ot,
                    o: o,
                    c: c
                });
            }
        },

        error: (t, o) => {
            let s;

            try {
                throw new Error();
            } catch (e) {
                s = e.stack;
            }

            log.say(`%c${t}\n\t\t%rat [${typeof o === 'object' ? (typeof o.prefix === 'string' ? o.prefix : 'app') : 'app'}]${s !== undefined ? (s.split('Object.do')[1] !== undefined ? (`:${parseInt(s.split('Object.do')[1].split(':')[1]) - 1}`) : '') : ''}`, {importance: 0, showPrefix: false, color: '#FE4A49', type: 'error'}, 'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#FE4A49;')
        },

        show: i => {
            i = i === undefined ? 0 : i;
            console.clear();

            lma.forEach(e => {
                if((typeof e.o === 'object' ? (e.o.importance !== undefined ? (e.o.importance <= i && e.o.importance >= 0) : true) : true) && (typeof e.o === 'object' ? (typeof e.o.remove === 'boolean' ? !e.o.remove : true) : true)) {
                    log.say(e.t, {...e.o, remove: true}, e.c);
                }
            });
        },

        showOnly: i => {
            i = i === undefined ? 0 : i;
            console.clear();

            lma.forEach(e => {
                if((typeof e.o === 'object' ? (e.o.importance !== undefined ? (e.o.importance === i && e.o.importance >= 0) : true) : true) && (typeof e.o === 'object' ? (typeof e.o.remove === 'boolean' ? !e.o.remove : true) : true)) {
                    log.say(e.t, {...e.o, remove: true}, e.c);
                }
            });
        },

        showAll: () => {
            hook.log.show(Infinity);
        },

        clear: () => {
            console.clear();

            lma.forEach(e => {
                if(typeof e.o === 'object' ? (typeof e.o.remove === 'boolean' ? !e.o.remove : true) : true) {
                    log.say(e.t, {...e.o, remove: true}, e.c);
                }
            });
        }
    };

    log.say(`Loaded version %c${localStorage.getItem('-hook-version--hook')}`, {prefix: 'hook.js', importance: 2}, 'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#3C40C6;');

    let sdi = [];
    let sdt = [];

    let s = {};
    let j = [];

    let cv = localStorage.getItem('-hook-version--data') !== null ? localStorage.getItem('-hook-version--data') : '-1';
    let hfr = true;

    let hurl = {};

    let dvm = localStorage.getItem('-dev') === null ? false : (localStorage.getItem('-dev') === 'true');

    window.location.search.substr(1).split('&').forEach(qp => {
        hurl[qp.split('=')[0]] = decodeURIComponent(qp.split('=')[1]).toString();
    });

    if(!dvm) {
        window.history.replaceState({}, document.title, '/');
    }

    if(dvm) {
        log.say('Developer mode activated', {prefix: 'hook.js', color: '#3235A3', importance: 1});
    }

    let rn = () => {
        [
            ...document.getElementsByTagName('script'),
            ...document.getElementsByTagName('link'),
            ...document.getElementsByTagName('img')
        ].forEach(e => {
            if(e.getAttribute('hook') !== null) {
                let set = e.getAttribute('hook').split('::')[0];
                let to = e.getAttribute('hook').split('::')[1];

                hook.get(to, (data, object) => {
                    if(object.t === 'string') {
                        if(e.tagName.toLowerCase() === 'script' && e.getAttribute('private')) {
                            hook.run(to, () => {});
                        } else {
                            let d = document.createElement(e.tagName === 'LINK' ? 'STYLE' : e.tagName);
                            d.classList.add(`hook::${to}`);
                            document.body.appendChild(d);

                            d.innerHTML = data;

                            e.remove();
                        }
                    } else if(object.t === 'image' || object.t === 'vector') {
                        let fd;

                        if(object.t === 'image') {
                            let bc = window.atob(data);
                            let bn = new Array(bc.length);

                            for(let i = 0; i < bc.length; i++) {
                                bn[i] = bc.charCodeAt(i);
                            }

                            fd = window.URL.createObjectURL(new Blob([new Uint8Array(bn)]))
                        } else {
                            fd = window.URL.createObjectURL(new Blob([data], {type: 'image/svg+xml'}))
                        }

                        e.setAttribute(set, data === '' ? '' : fd);
                        e.classList.add(`hook::${to}`);
                    }
                });
            }
        });
    };

    let rhcac = frd => {
        if (
            cv !== (frd.split(':').length === 2 ? frd.split(':')[0] : '-1')
        )  {
            g(
                (
                    document.querySelector('hook') !== undefined ?
                        (
                            document.querySelector('hook').getAttribute('src') !== undefined ?
                                document.querySelector('hook').getAttribute('src') : './data.hook'
                        ) : './data.hook'
                ), srd => {
                    if (hfr) {
                        localStorage.setItem('-hook--data', srd);

                        s = JSON.parse(localStorage.getItem('-hook--data'));
                        log.say(`Loading ${s.hook.length} components`, {prefix: 'hook.js', importance: 3});

                        s.hook.forEach(d => {
                            hook.resolve(d.name, (x) => {
                                j.push(x);

                                if (s.hook.length === j.length) {
                                    rn();
                                }
                            });
                        });

                        hfr = false;
                    } else {
                        JSON.parse(srd).hook.forEach((ud, ui) => {
                            if (ud.version !== s.hook[ui].version) {
                                hook.update(ud);
                            }
                        });

                        if(hook.dev) {
                            hook.reload();
                        }
                    }

                    localStorage.setItem('-hook-version--data', (frd.split(':').length === 2 ? frd.split(':')[0] : -1));

                    if(hfr) {
                        log.say(`Components version %c${localStorage.getItem('-hook-version--data')}`, {prefix: 'hook.js', importance: 2}, 'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#3C40C6;');
                    }

                    cv = frd.split(':').length === 2 ? frd.split(':')[0] : '-1';
                    log.say(`Updating components to version %c${localStorage.getItem('-hook-version--data')}`, {prefix: 'hook.js', importance: 2}, 'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#3C40C6;');
                }
            );
        } else if(hfr) {
            s = JSON.parse(localStorage.getItem('-hook--data'));

            if(hfr) {
                log.say(`Components version %c${localStorage.getItem('-hook-version--data')}`, {prefix: 'hook.js', importance: 2}, 'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#3C40C6;');
            }

            log.say(`Loading %c${s.hook.length} %rcomponents`, {prefix: 'hook.js', importance: 3}, 'letter-spacing:1.5px;font-weight:bold;font-family:sans-serif;font-size:1.2em;color:#3C40C6;');

            s.hook.forEach(d => {
                hook.resolve(d.name, (x) => {
                    j.push(x);

                    if (s.hook.length === j.length) {
                        rn();
                    }
                });
            });

            hfr = false;
        }
    };

    let rhca = () => {
        if(localStorage.getItem('-hook--version') === null ? false : (localStorage.getItem('-hook--version').length > 0)) {
            rhcac(localStorage.getItem('-hook--version'));
            localStorage.setItem('-hook--version', '');
        } else {
            g((
                document.querySelector('hook') !== undefined ?
                    (
                        document.querySelector('hook').getAttribute('version') !== undefined ?
                            document.querySelector('hook').getAttribute('version') : './version.hook'
                    ) : './version.hook'
            ), frd => {
                rhcac(frd);
            });
        }
    };

    setInterval(() => {
        rhca();
    }, dvm ? 500 : 15000);

    window.hook = {
        listen: (element, event, callback, renderingCondition, once) => {
            let di = true;

            if(element !== undefined) {
                if(typeof renderingCondition === 'object' ? (renderingCondition.filter(v => { return rlo.includes(v) }).length > 0) : true) {
                    let cbf = (ce) => {
                        if(di) {
                            callback(ce);

                            if(once) {
                                di = false;
                            }
                        } else {
                            element.removeEventListener(event, cbf);
                        }
                    };

                    element.addEventListener(event, cbf);
                }
            }

            return element === undefined;
        },

        clearIntervals: () => {
            sdi.forEach(ci => {
                clearInterval(ci);
            });
        },

        clearTimeouts: () => {
            sdt.forEach(ci => {
                clearTimeout(ci);
            });
        },

        interval: (callback, timeout) => {
            sdi.push(
                setInterval(callback, timeout)
            )
        },

        timeout: (callback, timeout) => {
            sdt.push(
                setTimeout(callback, timeout)
            );
        },

        setup: rn,

        clear: () => {
            localStorage.clear();
        },

        reload: () => {
            let urls = '/';

            Object.keys(hook.url.parameters).forEach((urlk, urli) => {
                if(typeof hook.url.parameters[urlk] === 'string' ? hook.url.parameters[urlk].length > 0 : false) {
                    if(urli > 0) {
                        urls += '&';
                    } else {
                        urls += '?';
                    }

                    urls += `${urlk}=${hook.url.parameters[urlk]}`;
                }
            });

            window.history.replaceState({}, document.title, urls);
            window.location.reload();
        },

        reset: () => {
            hook.removeAll();
            hook.clear();
            hook.url.clear();
            hook.reload();
        },

        load: (d, c) => {
            let q = new XMLHttpRequest();

            q.addEventListener('readystatechange', () => {
                if(q.status === 200 && q.readyState === 4) {
                    if(/\S*\s*\.png|\.jpg/g.test(s.hook[d.f].path)) {
                        let re = new FileReader();

                        re.addEventListener('loadend', () => {
                            let x = {
                                d: re.result.replace(/^data:.+;base64,/, ''),
                                n: d.n,
                                v: s.hook[d.f].version,
                                t: 'image',
                                p: s.hook[d.f].path
                            };

                            localStorage.setItem(d.n, JSON.stringify(x));
                            typeof c === 'function' ? c(x) : null;
                        });

                        re.readAsDataURL(q.response);
                    } else if(/\S*\s*\.svg/g.test(s.hook[d.f].path)) {
                        let x = {
                            d: q.responseText,
                            n: d.n,
                            v: s.hook[d.f].version,
                            t: 'vector',
                            p: s.hook[d.f].path
                        };

                        localStorage.setItem(d.n, JSON.stringify(x));
                        typeof c === 'function' ? c(x) : null;
                    } else {
                        let x = {
                            d: q.responseText,
                            n: d.n,
                            v: s.hook[d.f].version,
                            t: 'string',
                            p: s.hook[d.f].path
                        };

                        localStorage.setItem(d.n, JSON.stringify(x));
                        typeof c === 'function' ? c(x) : null;
                    }
                }
            });

            if(/\S*\s*\.png|\.jpg/g.test(s.hook[d.f].path)) {
                q.responseType = 'blob';
            }

            q.open('GET', s.hook[d.f].path);
            q.send();
        },

        remove: s => {
            if(document.getElementsByClassName(`hook::${s}`).length > 0) {
                document.getElementsByClassName(`hook::${s}`)[0].remove();

                return true;
            }

            return false;
        },

        removeAll: () => {
            j.forEach(ce => {
                hook.remove(ce.n);

                localStorage.removeItem(ce.n);
            });
        },

        run: (s, c, p) => {
            hook.get(s, (d, o) => {
                if(document.getElementsByClassName(`hook::${s}`).length > 0) {
                    hook.remove(s);
                }

                let e = document.createElement('script');

                o.callback = c;

                e.innerHTML = `
                    (() => {let __componentId = '${s}';let __parameters = JSON.parse(\`${typeof p === 'object' ? JSON.stringify(p).replace(/\\/g, '\\\\') : '{}'}\`);let done = (...args) => { ${typeof c === 'function' ? `hook.get('${s}', (d, o) => {o.callback(args);})` : null }};let log = {say: (t, o) => {hook.log.say(t, {prefix: __componentId, prefixColor: '#fa8231', delete: false, importance: (typeof o !== 'object' ? 4 : (typeof o.importance === 'number' ? o.importance : 4))});},error: (t, l) => {let fs = typeof o === 'object' ? (typeof o.prefix === 'string' ? o.prefix : 'app') : 'app';hook.log.error(t, {prefix: __componentId});},show: hook.log.show,clear: hook.log.clear};${d};})();
                `;

                e.classList.add(`hook::${s}`);
                document.body.appendChild(e);
            });
        },

        get: (f, c) => {
            j.forEach(jj => {
                if(jj.n === f) {
                    typeof c === 'function' ? c(jj.d, jj) : null;
                }
            });
        },

        resolve: (a, c) => {
            let f = -1;

            s.hook.forEach((h, i) => {
                if(h.name === a) {
                    f = i;
                }
            });

            if(f > -1) {
                let cd = window.localStorage.getItem(a);

                if (cd !== null && cd !== undefined) {
                    cd = JSON.parse(cd);

                    if(cd.v === s.hook[f].version) {
                        c(cd);
                    } else {
                        hook.load({
                            f: f,
                            n: a
                        }, x => {
                            typeof c === 'function' ? c(x) : null;
                        });
                    }
                } else {
                    hook.load({
                        f: f,
                        n: a
                    }, x => {
                        typeof c === 'function' ? c(x) : null;
                    });
                }
            }
        },

        update: c => {
            s.hook.forEach((ce, i) => {
                if(ce.name === c.name) {
                    hook.load({f: i, n: c.name}, (x) => {
                        j[i] = x;
                    });
                }
            });
        },

        log: log,

        url: {
            parameters: hurl,
            clear: () => {
                hook.url.parameters = {};
            }
        },

        dev: dvm
    };

    rhca();
})();