let Socket = (url) => {
    let pptt;
    let c = new WebSocket(url);

    let ou = onceupon();
    ou.create('connection');

    let e = (event, text) => {
        ou.on('connection', () => {
            c.send(`${event}::${typeof text}::${text !== undefined ? (typeof text === 'object' ? JSON.stringify(text) : text.toString()) : ''}`);
        });
    };

    c.addEventListener('open', () => {
        c.addEventListener('message', m => {
            let f = m.data.split('::');
            let l = '';

            switch(f[1]) {
                case 'object':
                    l = JSON.parse(f[2]);
                    break;
                case 'string':
                    l = f[2];
                    break;
                case 'number':
                    l = parseInt(f[2]);
                    break;
                case 'boolean':
                    l = (f[2] === 'true');
                    break;
                default:
                    l = null;
                    break;
            }

            ou.fire(f[0], l === '' ? null : l);
        });

        ou.fire('connection');
        wsr.connected = true;
    });

    c.addEventListener('error', (error) => {
        ou.fire('error', error);
    });

    let wsr = {
        on: ou.on,
        once: ou.once,

        emit: e,
        close: () => {
            ou.fire('timeout');
            c.close();
        },

        connected: false
    };

    wsr.on('ping', () => {
        ppt();

        wsr.emit('pong');
    });

    let ppt = () => {
        if(pptt !== undefined) {
            clearTimeout(pptt);
        }

        pptt = setTimeout(() => {
            wsr.connected = false;
            c.close();

            ou.fire('timeout');
        }, 25000);
    };

    return wsr;
};