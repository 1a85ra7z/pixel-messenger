let cpimage = (d, c) => {
    let r = new FileReader();

    r.addEventListener('load', (f) => {
        const i = new Image();
        i.src = f.target.result;

        i.addEventListener('load', () => {
            let x = 300 / i.naturalWidth;
            let e = document.createElement('canvas');
            let ctx = e.getContext('2d');

            e.width = i.naturalWidth * x;
            e.height = i.naturalHeight * x;

            ctx.drawImage(i, 0, 0, e.width, e.height);

            c(e.toDataURL());
        });
    });

    r.readAsDataURL(d);
};