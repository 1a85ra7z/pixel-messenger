'use strict';
let wl = false; window.addEventListener('load', () => { wl = true; });
let h = () => {
    let s = document.createElement('script');
    s.innerHTML = localStorage.getItem('-hook').length !== 0 ? localStorage.getItem('-hook') : '() => {}';
    let i = setInterval(() => {
        if(wl) {
            clearInterval(i);
            document.body.append(s);
        }
    })
};
let g = (f, c) => {
    let r = new XMLHttpRequest();
    r.addEventListener('readystatechange', () => {
        if(r.readyState === 4 && r.status === 200) {
            c(r.responseText);
        }
    });
    r.open('GET', f);
    r.send();
};
g('./version.hook', (d) => {
    localStorage.setItem('-hook--version', d);
    if(localStorage.getItem('-hook-version--hook') !== null ? (d.split(':').length === 2 ? (d.split(':')[1] !== localStorage.getItem('-hook-version--hook')) : false) : true) {
        g('./js/vendor/hook.js', (hc) => {
            localStorage.setItem('-hook', hc);
            localStorage.setItem('-hook-version--hook', (d.split(':').length === 2 ? d.split(':')[1] : '-1'));
            h();
        });
    } else {
        h();
    }
});