let formatdate = (d, f) => {
    let dy = [
        'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ];

    let mt = [
        'January', 'February', 'March', 'April', 'May', 'June', 'Juli', 'August', 'September', 'October', 'November', 'December'
    ];

    f = f.replace(
        /h/g,
        parseInt(d.getHours()) > 12 ? (parseInt(d.getHours()) - 12).toString() : d.getHours()
    );

    f = f.replace(
        /HH/g,
        d.getHours().toString()
    );

    f = f.replace(
        /dddd/g,
        dy[d.getDay()]
    );

    f = f.replace(
        /mmmm/g,
        mt[d.getMonth()]
    );

    f = f.replace(
        /dS/g,
        `${d.getDay()}th`
    );

    f = f.replace(
        /d/g,
        d.getDate().toString()
    );

    f = f.replace(
        /m/g,
        d.getMonth().toString()
    );

    f = f.replace(
        /yyyy/g,
        d.getFullYear().toString()
    );

    f = f.replace(
        /MM/g,
        parseInt(d.getMinutes()) < 10 ? `0${d.getMinutes().toString()}` : d.getMinutes().toString()
    );

    f = f.replace(
        /ss/g,
        d.getSeconds().toString()
    );

    f = f.replace(
        /TT/g,
        parseInt(d.getHours()) > 12 ? 'PM' : 'AM'
    );

    f = f.replace(
        /c/g,
        (
            (new Date().getDate() - d.getDate() > 1 && new Date().getMonth() - d.getMonth() > 0 && new Date().getFullYear() - d.getFullYear() > 0) ? (`${d.getDate()}. ${mt[d.getMonth()]} ${d.getFullYear()}`) :
                (new Date().getDate() - d.getDate() > 1) ? (`${d.getDate()}. ${mt[d.getMonth()]}`) :
                    (new Date().getDate() - d.getDate() === 1) ? ('Yesterday') :
                        'Today'
        )
    );

    return f;
};