let coc;
let cocid;
let cmcsm = {};
let coces = {};

let onc = ccd => {
    if(typeof ccd === 'object' ? (typeof ccd.users === 'object' ? (typeof ccd.users[ccd.users.indexOf(__parameters.id) === 0 ? 1 : 0] !== 'undefined') : false) : false) {
        s.emit('message--getUserData', ccd.users[ccd.users.indexOf(__parameters.id) === 0 ? 1 : 0]);

        s.once(`message-getUserData-response--${ccd.users[ccd.users.indexOf(__parameters.id) === 0 ? 1 : 0]}`, cmd => {
            let ctd;
            let ccpp;

            s.on(`settings-updateProfilePicture-response--${cmd.id}`, uppd => {
                if(typeof uppd === 'string' && typeof ctd !== 'undefined') {
                    ccpp = uppd;
                    
                    ctd.querySelector('.app-sidebar-chats-container-chat--picture').setAttribute('src', ccpp);
                    
                    if(cocid === cmd.id) {
                        document.querySelector('.app-chat-header-user--picture').setAttribute('src', ccpp);
                    }
                }
            });

            s.emit('message--getLastMessage', ccd.meta.id);

            s.on(`message-getLastMessage-response--${ccd.meta.id}|message-new--${ccd.meta.id}`, (cmlmd, e) => {
                let o;

                if(e === `message-new--${ccd.meta.id}` && typeof cmcsm[ccd.meta.id] === 'object') {
                    o = cmcsm[ccd.meta.id].classList.contains('-state--opened');
                    cmcsm[ccd.meta.id].remove();
                }

                if(typeof cmlmd === 'object') {
                    if(typeof cmd.picture === 'string' && typeof cmd.picture === 'string') {
                        ccpp = cmd.picture;

                        render.component('component:chat', {
                            picture: ccpp,
                            name: cmd.name,
                            message: typeof cmlmd.data === 'string' ? cmlmd.data : '<i>No messages available</i>'
                        }, ctdun => {
                            if(e === `message-getLastMessage-response--${ccd.meta.id}` && Object.entries(cmlmd).length > 0) {
                                if(cmlmd.meta.state < 2 && cmlmd.by !== __parameters.id) {
                                    ctdun.classList.add('-state--unread');
                                }
                            }

                            ctd = ctdun;

                            cmcsm[ccd.meta.id] = ctd;
                            coces[ccd.meta.id] = () => {
                                if(coc !== ccd.meta.id) {
                                    cocid = cmd.id;
                                    coc = ccd.meta.id;

                                    [...document.querySelector('.app-chat--main').children].forEach(mce => {
                                        mce.remove();
                                    });

                                    ctd.classList.add('-state--opened');

                                    document.querySelector('.app-chat-header-user--name').innerHTML = cmd.name;
                                    document.querySelector('.app-chat-header-user--picture').setAttribute('src', ccpp);

                                    if(document.querySelector('.app-chat--header').classList.contains('-state--hidden')) {
                                        document.querySelector('.app-chat--header').classList.remove('-state--hidden')
                                    }

                                    if(document.querySelector('.app-chat--write').classList.contains('-state--hidden')) {
                                        document.querySelector('.app-chat--write').classList.remove('-state--hidden')
                                    }

                                    hook.run('js:write', wc => {
                                        s.emit('message--write', {
                                            c: ccd.meta.id,
                                            m: wc[0]
                                        });

                                        s.on('message-write--response', mwr => {
                                            if(mwr === 'error-message--chatNotFound') {
                                                log.error('Chat not found')
                                            } else if(mwr === 'error-message--noAccess') {
                                                log.error('Can\'t access chat')
                                            } else if(mwr === 'message-write-response--success') {
                                                log.say('Sent message')
                                            }
                                        })
                                    });

                                    s.emit('message--getMessages', ccd.meta.id);

                                    s.on(`message-getMessages-response--${ccd.meta.id}|message-new--${ccd.meta.id}`, (m, e) => {
                                        if(typeof m === 'object') {
                                            if(m.type === 'text') {
                                                render.component('component:message', {
                                                    message: m.data,
                                                    time: formatdate(new Date(m.meta.time), 'HH:MM')
                                                }, cmd => {
                                                    if(typeof cmd === 'object') {
                                                        cmd.classList.add(m.by === __parameters.id ? '-state--right' : '-state--left');

                                                        if(e === `message-new--${ccd.meta.id}`) {
                                                            document.querySelector('.app-chat--main').append(cmd);
                                                        } else {
                                                            document.querySelector('.app-chat--main').prepend(cmd);
                                                        }
                                                    }
                                                });
                                            } else if(m.type === 'time') {
                                                render.component('component:time', {
                                                    time: formatdate(new Date(m.time), 'c')
                                                }, cmd => {
                                                    if(typeof cmd === 'object') {
                                                        if(e === `message-new--${ccd.meta.id}`) {
                                                            document.querySelector('.app-chat--main').append(cmd);
                                                        } else {
                                                            document.querySelector('.app-chat--main').prepend(cmd);
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            };

                            if(o) {
                                ctd.classList.add('-state--opened');
                            }

                            if(typeof ctd === 'object') {
                                if(typeof cmlmd.meta === 'object' ? (cmlmd.meta.state === 1 && cmlmd.by !== __parameters.id) : false) {
                                    ctd.classList.add('-state--unread');
                                }

                                hook.listen(ctd, 'click', () => {
                                    coces[ccd.meta.id]();
                                    s.emit('message--read', ccd.meta.id);
                                }, ['page:app']);

                                document.querySelector('.app-sidebar-chats--container').appendChild(ctd);
                            }
                        });
                    }
                } else if(typeof cmlmd === 'string') {
                    if(cmlmd === 'error-message--chatNotFound') {
                        log.error('Chat not found')
                    } else if(cmlmd === 'error-message--noAccess') {
                        log.error('Can\'t access chat')
                    }
                }
            });
        });
    } else if(typeof ccd === 'string') {
        if(ccd === 'error-message--chatNotFound') {
            log.error('Chat not found')
        } else if(ccd === 'error-message--noAccess') {
            log.error('Can\'t access chat')
        }
    }
};

s.emit('message--getChats');

s.on('message-getChats--response', d => {
    if(typeof d === 'object') {
        document.querySelector('.app-sidebar-chats-header--count').innerHTML = d.length.toString();

        d.forEach(cc => {
            s.emit('message--getChatData', cc);
        });

        s.on('message-getChatData--response', ccd => {
            onc(ccd);
        });
    } else {
        log.error('Invalid type of socket response');
    }
});

s.on('message-newChat--response', d => {
    if(typeof d === 'object') {
        onc(d, 0);

        document.querySelector('.app-sidebar-chats-header--count').innerHTML = (
            parseInt(document.querySelector('.app-sidebar-chats-header--count').innerHTML) + 1
        ).toString();
    }
});

s.on('message--openChat', d => {
    if(typeof d === 'string' ? typeof coces[d] === 'function' : false) {
        coces[d]();
    }
});