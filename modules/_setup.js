const logger = require('../vendor/logger');
const colorify = require('../vendor/colorify');
const unserve = require('../vendor/unserve/unserve');

let options = {
    listen: '../web/',
    port: 3465,

    socket: 8998,

    ssl: {
        key: './ssl/privateKey.pem',
        cert: './ssl/certificate.pem'
    }
};

let app = unserve(options);

const BasementClient = require('../vendor/bsmntdb')
    .BasementClient('./data');

module.exports = (c) => {
    logger.info(`Setting ${colorify.blue('HTTP/HTTPS & Socket Server')} ${colorify.white('up.')}`);

    app.once('ready', () => {
        logger.info(`Setting ${colorify.blue('BasementDB')} ${colorify.white('up.')}`);

        BasementClient.once('ready', () => {
            logger.success('Done.');

            logger.info(`HTTP Server running on port ${colorify.yellow(`${options.port}`)}${colorify.white('.')}`);
            logger.info(`HTTPS Server running on port ${colorify.yellow(`${options.port + 1}`)}${colorify.white('.')}`);
            logger.info(`Socket Server running on port ${colorify.yellow(`${options.socket}`)}${colorify.white('.')}`);

            c({
                _db: BasementClient._data,
                _st: new Date(),
                _app: app
            });
        });
    });
};