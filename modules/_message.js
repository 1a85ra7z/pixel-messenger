const security = require('../vendor/security');
const formatdate = require('../vendor/formatdate');

module.exports = (_session, _socket, _currentUser) => {
    _socket.on('message--getChats', () => {
        if(typeof _currentUser.set('chats').chats === 'undefined') {
            _currentUser.set('chats').chats = [];
        }

        _socket.emit('message-getChats--response', _currentUser.set('chats').chats);
    });

    _socket.on('message--getChatData', d => {
        if(typeof d === 'string' ? (typeof _session._db.field('chats').field(d).set('chat').users !== 'undefined') : false) {
            if(_session._db.field('chats').field(d).set('chat').users.includes(_currentUser.set('profile').u)) {
                _socket.emit('message-getChatData--response', _session._db.field('chats').field(d).set('chat'));
            } else {
                _socket.emit('message-getChatData--response', 'error-message--noAccess');
            }
        } else {
            _socket.emit('message-getChatData--response', 'error-message--chatNotFound');
        }
    });

    _socket.on('message--getUserData', d => {
        if(typeof d === 'string' ? d.length === 9 : false) {
            let ud = _session._db.field('users').set('data');

            _socket.emit(`message-getUserData-response--${d}`, {
                name:
                    Object.keys(
                        ud
                    ).find(
                        key => ud[key] === d
                    ),
                cid: _session._db.field('users').field(d).set('profile').cid,
                picture: _session._db.field('users').field(d).set('profile').p,
                id: _session._db.field('users').field(d).set('profile').u
            });
        }
    });

    _socket.on('message--getUsername', d => {
        if(typeof d === 'string' ? d.length === 9 : false) {
            if(typeof _session._db.field('users').field(d).set('profile').n === 'string' && d !== _currentUser.set('profile').u) {
                _socket.emit(`message-getUsername-response--${d}`, _session._db.field('users').field(d).set('profile').n);
            } else if(d === _currentUser.set('profile').u) {
                _socket.emit(`message-getUsername-response--${d}`, 'error-message--ownId');
            } else {
                _socket.emit(`message-getUsername-response--${d}`, 'error-message--invalidId');
            }
        } else {
            _socket.emit(`message-getUsername-response--${d}`, 'error-message--invalidId');
        }
    });

    _socket.on('message--getLastMessage', d => {
        if(typeof d === 'string' ? (typeof _session._db.field('chats').field(d).set('chat').users !== 'undefined') : false) {
            if(_session._db.field('chats').field(d).set('chat').users.includes(_currentUser.set('profile').u)) {
                _socket.emit(`message-getLastMessage-response--${d}`,
                    _session._db.field('chats').field(d).field('messages').set(
                        _session._db.field('chats').field(d).field('messages').set('messages').count.toString()
                    )
                );
            } else {
                _socket.emit(`message-getLastMessage-response--${d}`, 'error-message--noAccess');
            }
        } else {
            _socket.emit(`message-getLastMessage-response--${d}`, 'error-message--chatNotFound');
        }
    });

    _socket.on('message--read', d => {
        let i = _session._db.field('chats').field(d).field('messages').set('messages').count;

        while(i > 0) {
            if(_session._db.field('chats').field(d).field('messages').set(i).by !== _currentUser.set('profile').u) {
                _session._db.field('chats').field(d).field('messages').set(i).meta = {
                    ..._session._db.field('chats').field(d).field('messages').set(i).meta, ...{state: 2}
                }
            }

            i--;
        }
    });

    _socket.on('message--getMessages', d => {
        if(typeof d === 'string' ? (typeof _session._db.field('chats').field(d).set('chat').users !== 'undefined') : false) {
            if(_session._db.field('chats').field(d).set('chat').users.includes(_currentUser.set('profile').u)) {
                let cd;
                let i = _session._db.field('chats').field(d).field('messages').set('messages').count;

                if(i > 0) {
                    cd = formatdate(new Date(_session._db.field('chats').field(d).field('messages').set(i).meta.time), 'd.m.yyyy');
                }

                while(i > 0) {
                    if(formatdate(new Date(_session._db.field('chats').field(d).field('messages').set(i).meta.time), 'd.m.yyyy') !== cd) {
                        _socket.emit(`message-getMessages-response--${d}`, {
                            type: 'time',
                            time: cd
                        });

                        cd = formatdate(new Date(_session._db.field('chats').field(d).field('messages').set(i).meta.time), 'd.m.yyyy');
                    }

                    _socket.emit(`message-getMessages-response--${d}`,
                        _session._db.field('chats').field(d).field('messages').set(i)
                    );

                    i--;
                }

                if(_session._db.field('chats').field(d).field('messages').set('messages').count > 0) {
                    _socket.emit(`message-getMessages-response--${d}`, {
                        type: 'time',
                        time: _session._db.field('chats').field(d).field('messages').set(1).meta.time
                    });
                }
            } else {
                _socket.emit(`message-getMessages-response--${d}`, 'error-message--noAccess');
            }
        } else {
            _socket.emit(`message-getMessages-response--${d}`, 'error-message--chatNotFound');
        }
    });

    _socket.on('message--write', d => {
        if(typeof d === 'object' ? (typeof d.c === 'string' ? (typeof _session._db.field('chats').field(d.c).set('chat').users !== 'undefined') : false) : false) {
            if(_session._db.field('chats').field(d.c).set('chat').users.includes(_currentUser.set('profile').u)) {
                if(typeof d.m === 'object' ? (typeof d.m.type === 'string' && typeof d.m.data === 'string') : false) {
                    let i = ++_session._db.field('chats').field(d.c).field('messages').set('messages').count;

                    _session._db.field('chats').field(d.c).field('messages').set(i).by = _currentUser.set('profile').u;
                    _session._db.field('chats').field(d.c).field('messages').set(i).type = d.m.type;
                    _session._db.field('chats').field(d.c).field('messages').set(i).data = d.m.data;

                    _session._db.field('chats').field(d.c).field('messages').set(i).meta = {
                        id: security.salt(16),
                        time: new Date().toString(),
                        state: 0
                    };

                    _session._db.field('chats').field(d.c).set('chat').users.forEach(csus => {
                       if(typeof _session._connections[csus] === 'object') {
                           if(
                               typeof _session._db.field('chats').field(d.c).field('messages').set(i - 1) === 'object' &&
                               (
                                   (
                                       i > 1 ? (
                                           formatdate(
                                               new Date(_session._db.field('chats').field(d.c).field('messages').set(i - 1).meta.time), 'd.m.yyyy'
                                           ) !== formatdate(
                                               new Date(_session._db.field('chats').field(d.c).field('messages').set(i).meta.time), 'd.m.yyyy'
                                           )
                                       ) : false
                                   ) || i === 1
                               )
                           ) {
                               _session._connections[csus].emit('message--new', {
                                   type: 'time',
                                   time: _session._db.field('chats').field(d.c).field('messages').set(i).meta.time
                               });
                           }

                           _session._connections[csus].emit(`message-new--${_session._db.field('chats').field(d.c).field('messages').set(i)}`, _session._db.field('chats').field(d.c).field('messages').set(i));
                       }
                    });

                    _socket.emit('message-write--response', 'message-write-response--success');
                }
            } else {
                _socket.emit('message-write--response', 'error-message--noAccess');
            }
        } else {
            _socket.emit('message-write--response', 'error-message--chatNotFound');
        }
    });

    _socket.on('message--newChat', t => {
        if(typeof t === 'string') {
            if((typeof _currentUser.set('chats').users === 'object' ? !_currentUser.set('chats').users.includes(t) : true) && t !== _currentUser.set('profile').u) {
                if(typeof _currentUser.set('chats').users !== 'object') {
                    _currentUser.set('chats').users = [];
                }

                if(_session._db.field('users').field(t).set('profile') !== undefined) {
                    let chid = security.id(16).toLowerCase();

                    _currentUser.set('chats').users = [..._currentUser.set('chats').users, t];
                    _currentUser.set('chats').chats = [..._currentUser.set('chats').chats, chid];

                    _session._db.field('users').field(t).set('chats').users = [..._session._db.field('users').field(t).set('chats').users, _currentUser.set('profile').u];
                    _session._db.field('users').field(t).set('chats').chats = [..._session._db.field('users').field(t).set('chats').chats, chid];

                    _session._db.field('chats').field(chid).set('chat').users = [t, _currentUser.set('profile').u];
                    _session._db.field('chats').field(chid).set('chat').meta = {
                        time: new Date().toString(),
                        id: chid
                    };

                    _session._db.field('chats').field(chid).field('messages').set('messages').count = 0;

                    _session._db.field('chats').field(chid).set('chat').users.forEach(csus => {
                        if(typeof _session._connections[csus] === 'object') {
                            _session._connections[csus].emit('message-newChat--response', _session._db.field('chats').field(chid).set('chat'));
                            _session._connections[csus].emit('message--openChat', chid);
                        }
                    });
                }
            } else if(typeof _currentUser.set('chats').users === 'object' && t !== _currentUser.set('profile').u) {
                _socket.emit('message--openChat', _currentUser.set('chats').chats[_currentUser.set('chats').users.indexOf(t)]);
            }
        }
    });
};