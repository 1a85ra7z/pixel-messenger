const security = require('../vendor/security');
const picturize = require('../vendor/picturize');

let nsc = (_session, s, cu) => {
    let sa = security.salt(64);

    if(_session._db.field('users').set('auto').users === undefined) {
        _session._db.field('users').set('auto').users = [];
    }

    let fal = -1;

    _session._db.field('users').set('auto').users.forEach((calu, cli) => {
        if(cu === calu.cu) {
            fal = cli;
        }
    });

    let cra = _session._db.field('users').set('auto').users;

    if(fal > -1) {
        cra.splice(fal, 1);
    }

    cra.push({
        cu: cu,
        sa: sa
    });

    _session._db.field('users').set('auto').users = cra;

    s.emit('authentication--token', sa);
};

let lgdi = (s, cu, callback) => {
    s.emit('authentication--loggedIn');
    callback(cu);
};

module.exports = (_session, s, callback) => {
    s.emit('authentication');

    s.on('authentication', d => {
        if(
            (
                (d !== undefined && d !== null) ?
                    typeof d === 'object' ?
                        (
                            (
                                (d.u !== undefined && d.u !== null) &&
                                (d.p !== undefined && d.p !== null)
                            ) ?
                            (
                                (
                                    typeof d.u === 'string' &&
                                    typeof d.p === 'string'
                                ) ? (
                                    (d.u.length > 0) &&
                                    (d.p.length > 0)
                                ) : false
                            ) : false
                        )
                    : false
                : false
            )
        ) {
            if(_session._db.field('users').set('data')[d.u] === undefined) {
                let cd = security.sha512(d.p, security.salt(16));
                let ppc = ['#FE4A49', '#30303A', '#fa8231', '#05c46b'];

                _session._db.field('users').set('data')[d.u] = Math.floor(Math.random() * (999999999 - 100000000 + 1) + 100000000).toString();

                let cu = _session._db.field('users').field(
                    _session._db.field('users').set('data')[d.u]
                );

                cu.set('authentication').d = cd.d;
                cu.set('authentication').s = cd.s;

                cu.set('profile').u = _session._db.field('users').set('data')[d.u];
                cu.set('profile').p = picturize(d.u.substr(0, 1), ppc[Math.floor(Math.random() * ppc.length)]);
                cu.set('profile').n = d.u;
                cu.set('profile').fn = '';
                cu.set('profile').em = '';
                cu.set('profile').dp = true;

                cu.set('chats').chats = [];
                cu.set('chats').users = [];

                if(d.s) {
                    nsc(_session, s, _session._db.field('users').set('data')[d.u]);
                }

                lgdi(s, cu, callback);
            } else {
                let cu = _session._db.field('users').field(
                    _session._db.field('users').set('data')[d.u]
                );

                if(cu.set('authentication').d === security.sha512(d.p, cu.set('authentication').s).d) {
                    if(d.s) {
                        nsc(_session, s, _session._db.field('users').set('data')[d.u]);
                    }

                    lgdi(s, cu, callback);
                } else {
                    s.emit('authentication--wrongPassword');
                }
            }
        }
    });

    s.once('authentication--autoLogin', d => {
        if(typeof d === 'string' ? d.length === 64 : false) {
            if(_session._db.field('users').set('auto').users === undefined) {
                _session._db.field('users').set('auto').users = [];
            }

            let fal = -1;

            _session._db.field('users').set('auto').users.forEach((calu, cli) => {
                if(d === calu.sa) {
                    fal = cli;
                }
            });

            if(fal !== -1) {
                let cun = _session._db.field('users').set('auto').users[fal].cu;

                nsc(_session, s, cun);

                lgdi(s, _session._db.field('users').field(
                    cun
                ), callback);
            } else {
                s.emit('authentication--autoLoginFailed');
            }
        } else {
            s.emit('authentication--autoLoginFailed');
        }
    });
};