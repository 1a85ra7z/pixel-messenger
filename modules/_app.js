let _message = require('./_message');
let _settings = require('./_settings');
const security = require('../vendor/security');

module.exports = (_session, _socket, _currentUser) => {
    _socket.on('app--getUserData', () => {
        let ud = _session._db.field('users').set('data');

       _socket.emit('app--getUserData', {
           name:
               Object.keys(
                   ud
               ).find(
                   key => ud[key] === _currentUser.set('profile').u
               ),
           cid: _currentUser.set('profile').cid,
           picture: _currentUser.set('profile').p,
           id: _currentUser.set('profile').u,
           fullName: _currentUser.set('profile').fn,
           email: _currentUser.set('profile').em
       });
    });

    _socket.on('lock--token', () => {
        let ud = _session._db.field('users').set('data');

        _socket.emit('lock--token', Object.keys(
            ud
        ).find(
            key => ud[key] === _currentUser.set('profile').u
        ),);
    });

    _message(_session, _socket, _currentUser);
    _settings(_session, _socket, _currentUser);
};