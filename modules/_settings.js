const sharp = require('sharp');
const picturize = require('../vendor/picturize');

module.exports = (_session, _socket, _currentUser) => {
    let ppc = ['#FE4A49', '#30303A', '#fa8231', '#05c46b'];

    _socket.on('settings--resetProfilePicture', () => {
        if(!_currentUser.set('profile').dp) {
            _currentUser.set('profile').p = picturize(_currentUser.set('profile').n.substr(0, 1), ppc[Math.floor(Math.random() * ppc.length)]);
            _socket.emit('settings-updateProfilePicture--response', _currentUser.set('profile').p);

            if(typeof _currentUser.set('chats').users === 'object') {
                _currentUser.set('chats').users.forEach(csus => {
                    if(typeof _session._connections[csus] === 'object') {
                        _session._connections[csus].emit(`settings-updateProfilePicture-response--${_currentUser.set('profile').u}`, _currentUser.set('profile').p);
                    }
                })
            }

            _currentUser.set('profile').dp = true;
        }
    });

    _socket.on('settings--updateFullName', d => {
        if(typeof d === 'string') {
            if(d.replace(/[^a-zA-Z\-\W]/g, '').length > 0) {
                _currentUser.set('profile').fn = d;
            }
        }
    });

    _socket.on('settings--updateEmail', d => {
        if(typeof d === 'string') {
            if(/^\S+@\S+$/.test(d)) {
                _currentUser.set('profile').em = d;
            }
        }
    });

    _socket.on('settings--updateProfilePicture', d => {
        if(typeof d === 'string') {
            let bf = Buffer.from(d.replace(/^data:image\/[a-zA-Z]+;base64,/, ''), 'base64');

            sharp(bf).metadata()
                .then((m) => {
                    let mx;
                    let l = 0;
                    let t = 0;

                    if (m.width > m.height) {
                        mx = m.height;
                        l = Math.floor((m.width - m.height) / 2);
                    } else if (m.width < m.height) {
                        mx = m.width;
                        t = Math.floor((m.height - m.width) / 2);
                    } else {
                        mx = m.width;
                    }

                    return sharp(bf)
                        .extract({width: mx, height: mx, left: l, top: t})
                        .toBuffer();
                })
                .then((rd) => {
                    _currentUser.set('profile').p = d.replace(d.replace(/^data:image\/[a-zA-Z]+;base64,/, ''), '') + rd.toString('base64');
                    _socket.emit('settings-updateProfilePicture--response', _currentUser.set('profile').p);

                    _currentUser.set('profile').dp = false;

                    if(typeof _currentUser.set('chats').users === 'object') {
                        _currentUser.set('chats').users.forEach(csus => {
                            if(typeof _session._connections[csus] === 'object') {
                                _session._connections[csus].emit(`settings-updateProfilePicture-response--${_currentUser.set('profile').u}`, _currentUser.set('profile').p);
                            }
                        })
                    }
                });
        }
    });
};