FROM node:lts-slim

RUN mkdir -p /srv/app/pixel-messenger
WORKDIR /srv/app/pixel-messenger

COPY package.json /srv/app/pixel-messenger
COPY package-lock.json /srv/app/pixel-messenger

RUN npm install

COPY . /srv/app/pixel-messenger

CMD ["npm", "run", "start"]