const fs = require('fs');
const Path = require('path');
let onceupon = require('onceupon.js')();

let e = {
    BasementClient: path => {
        let r = {
            on: onceupon.on,
            once: onceupon.once,

            data: {}
        };

        let tp = (o, p) => {
            return new Proxy(o, {
                get: (target, key) => {
                    if(target[key] === undefined && typeof key === 'string') {
                        if(key !== 'inspect') {
                            fs.mkdirSync(Path.resolve(`${p}/${key}/`));
                            fs.writeFileSync(Path.resolve(`${p}/${key}/basement.json`), '{}');

                            target[key] = tp(w(`${p}/${key}/`), `${p}/${key}/`);

                            return target[key];
                        }
                    }

                    return target[key];
                }, set: (target, key, value) => {
                    target[key] = value;
                    fs.writeFileSync(`${p}/basement.json`, JSON.stringify(target));

                    return true;
                }
            });
        };

        let w = p => {
            let o = {};

            fs.readdirSync(Path.resolve(p)).forEach(cf => {
                if(!cf.includes('.')) {
                    o[cf] = tp(w(Path.resolve(`${p}/${cf}/`)), `${p}/${cf}/`);
                } else {
                    o = {...o, ...JSON.parse(fs.readFileSync(Path.resolve(`${p}/${cf}`), 'UTF-8'))};
                }
            });

            return o;
        };

        onceupon.create('ready');
        r.data = tp(w(path), path);

        onceupon.fire('ready');

        return r;
    }
};

module.exports = e;