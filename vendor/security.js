const crypto = require('crypto');

module.exports = {
    id: len => {
        let fs = '';
        let i = Math.floor(len / 10) + 1;

        while(i !== 0) {
            fs += Math.random().toString(36).substr(2, 10);
            i--;
        }

        return fs.substring(0, len).toUpperCase();
    },

    salt: len => {
        return crypto.randomBytes(Math.ceil(len / 2))
            .toString('hex')
            .slice(0, len);
    },

    sha512: (d, s) => {
        let h = crypto.createHmac('sha512', s);

        h.update(d);

        return {
            s: s,
            d: h.digest('hex')
        };
    }
};