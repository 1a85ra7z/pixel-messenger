const { createCanvas } = require('canvas');

module.exports = (text, color) => {
    let canvas = createCanvas(300, 300);
    let ctx = canvas.getContext('2d');

    ctx.font = '150px Arial';

    ctx.fillStyle = color;

    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.fillStyle = '#FFFFFF';
    ctx.textAlign = 'center';
    ctx.fillText(text, 150, 200);

    return canvas.toDataURL('image/png');
};