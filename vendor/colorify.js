let p = (t, c) => {
    return `\x1b[${c.toString()}m${t}\x1b[0m`;
};

module.exports = {
    black: t => {
        return p(t, 30);
    },

    white: t => {
        return p(t, 97);
    },

    grey: t => {
        return p(t, 90);
    },

    red: t => {
        return p(t, 31);
    },

    green: t => {
        return p(t, 32);
    },

    yellow: t => {
        return p(t, 33);
    },

    blue: t => {
        return p(t, 34);
    },

    right: t => {
        return t.toString().padStart(process.stdout.columns - t.length);
    },

    center: t => {
        return t.toString().padStart((process.stdout.columns - t.length) / 2);
    },
    
    string: (option, text) => {
        option = option.toLowerCase();

        switch (option) {
            case 'black':
                return module.exports.black(text);
            case 'white':
                return module.exports.white(text);
            case 'grey':
                return module.exports.grey(text);
            case 'red':
                return module.exports.red(text);
            case 'green':
                return module.exports.green(text);
            case 'yellow':
                return module.exports.yellow(text);
            case 'blue':
                return module.exports.blue(text);
            case 'center':
                return module.exports.center(text);
            case 'right':
                return module.exports.right(text);
            default:
                return text;
        }
    }
};