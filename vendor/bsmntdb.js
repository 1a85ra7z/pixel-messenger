const fs = require('fs');
const Path = require('path');
let onceupon = require('onceupon.js')();

let Set = (path) => {
    let o = {};

    if(fs.existsSync(path) ? fs.lstatSync(path).isFile() : false) {
        let d = fs.readFileSync(path, 'UTF-8');

        if (d) {
            try {
                o = JSON.parse(d);
            } catch (e) {

            }
        }
    }

    return new Proxy(o, {
        set: (target, p, value) => {
            target[p] = value;

            if(!fs.existsSync(path) ? true : !fs.lstatSync(path).isFile()) {
                fs.writeFileSync(path, '{}');
            }

            fs.writeFileSync(path, JSON.stringify(target));
        }
    })
};

let Field = (path) => {
    if(!fs.existsSync(path) ? true : !fs.lstatSync(path).isDirectory()) {
        fs.mkdirSync(path);
    }

    return n(path);
};

let n = path => {
    return {
        set: (name) => {
            return Set(Path.resolve(`${path}/${name}.bsmnt`));
        },

        field: (name) => {
            return Field(Path.resolve(`${path}/${name}/`));
        }
    };
};

module.exports = {
    BasementClient: path => {
        onceupon.create('ready');
        onceupon.fire('ready');

        return {
            on: onceupon.on,
            once: onceupon.once,
            _data: n(path)
        };
    },
};