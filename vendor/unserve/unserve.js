const fs = require('fs');

const onceupon = require('onceupon.js')();
const http = require('./http');
const socket = require('./socket');

module.exports = (options) => {
    let ssl = {};

    if(options.ssl !== undefined) {
        ssl = {
            key: fs.readFileSync(options.ssl.key).toString(),
            cert: fs.readFileSync(options.ssl.cert).toString()
        }
    }

    onceupon.create('connection');

    let _http = http('./http/', options.port !== undefined ? parseInt(options.port) : 8080, ssl);
    let _socket = socket(options.socket !== undefined ? parseInt(options.socket) : 443, ssl);

    _socket.on('connection', d => {
       onceupon.fire('connection', d);
    });

    onceupon.create('ready');
    onceupon.fire('ready');

    return {
        once: onceupon.once,
        on: onceupon.on,

        _http: _http,
        _socket: _socket
    }
};