const https = require('https');
const http = require('http');

const url = require('url');
const path = require('path');
const fs = require('fs');

module.exports = (pth, port, ssl) => {
    let excpt = [];
    let ecnt = false;

    let _https;
    let _http;

    let h = (req, res) => {
        if(!ecnt) {
            let fexpt = -1;

            excpt.forEach((cext, iext) => {
                if(cext.u.test(req.url)) {
                    fexpt = iext;
                }
            });

            if(fexpt === -1) {
                let f = path.join(process.cwd(), pth, url.parse(req.url).pathname);

                fs.exists(f, (e) => {
                    if(!e) {
                        res.writeHead(404, {'Content-Type': 'text/plain'});
                        res.write('404 - Not found');
                        res.end();

                        return;
                    }

                    if(fs.statSync(f).isDirectory()) f += '/index.html';

                    fs.readFile(f, 'binary', (err, d) => {
                        if(err) {
                            res.writeHead(500, {'Content-Type': 'text/plain'});
                            res.write(err + '\n');
                            res.end();

                            return;
                        }

                        res.writeHead(200);
                        res.write(d, 'binary');
                        res.end();
                    });
                });
            } else {
                excpt[fexpt].c({
                    url: req.url,

                    emit: (data, type) => {
                        res.writeHead(excpt[fexpt].h.n, excpt[fexpt].h.o);
                        res.write(data !== undefined ? data : '', type !== undefined ? type : 'binary');
                        res.end();
                    },

                    head: (status, header) => {
                        if(typeof status === 'number') {
                            excpt[fexpt].h.n = status;
                        }

                        if(typeof header === 'object') {
                            excpt[fexpt].h.o = header;
                        }
                    }
                });
            }
        } else {
            res.end();
            req.connection.end();
            req.connection.destroy();
        }
    };

    if(ssl !== undefined) {
        if(typeof ssl.cert === 'string' && typeof ssl.key === 'string') {
            _https = https.createServer(ssl, h);
            _https.listen(port + 1);
        }
    }

    _http = http.createServer(h);
    _http.listen(port);

    return {
        https: _https,
        http: _http,

        except: (url, callback) => {
            excpt.push({
                u: new RegExp(url.replace(/\*/g, '\\w\*')),
                c: callback,
                h: {
                    n: 200,
                    o: {}
                }
            })
        },

        close: callback => {
            let cls = 0;
            ecnt = true;

            let clb = () => {
                cls++;

                if(cls === 2) {
                    callback();
                }
            };

            _https.close(() => {
                clb();
            });

            _http.close(() => {
                clb();
            });
        }
    };
};