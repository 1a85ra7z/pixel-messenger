const readline = require('readline');
let onceupon = require('onceupon.js')();

module.exports = () => {
    let rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.on('line', i => {
        onceupon.fire(i);
    });

    return {
        on: onceupon.on,
        once: onceupon.once
    }
};