const colorify = require('./colorify');
const formatDate = require('./formatdate');

let p = (t, c, o) => {
    let so = '';

    let cp = colorify.string(c, '➜  ') +
        colorify.grey('[') +
        formatDate(new Date(), 'HH:MM') +
        colorify.grey(']') +
        colorify.grey(': ');

    if(o !== undefined) {
        if(o.clean !== undefined ? (o.clean) : false) {
            cp = '';
        }

        if(o.align !== undefined) {
            so = o.align;
        }
    }

    console.log(
        colorify.string(
            so,

            cp +
            colorify.string(c, t)
        )
    )
};

module.exports = {
    warning: (t, o) => {
        p(t, 'yellow', o);
    },

    clear: () => {
        console.log('\033c');
    },

    error: (t, o) => {
        p(t, 'red', o);
    },

    info: (t, o) => {
        p(t, 'white', o);
    },

    success: (t, o) => {
        p(t, 'green', o);
    }
};